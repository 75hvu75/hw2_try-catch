const books = [
    {
        author: 'Люсі Фолі',
        name: 'Список запрошених',
        price: 70,
    },
    {
        author: 'Сюзанна Кларк',
        name: 'Джонатан Стрейндж і м-р Норрелл',
    },
    {
        name: 'Дизайн. Книга для недизайнерів.',
        price: 70,
    },
    {
        author: 'Алан Мур',
        name: 'Неономікон',
        price: 70,
    },
    {
        author: 'Террі Пратчетт',
        name: 'Рухомі картинки',
        price: 40,
    },
    {
        author: 'Анґус Гайленд',
        name: 'Коти в мистецтві',
    },
];

const rootDiv = document.getElementById('root');
const bookList = createBookList(books);
rootDiv.appendChild(bookList);

function createBookList(books) {
    const bookList = document.createElement('ul');

    books.forEach(book => {
        try {
			const requiredProps = ['author', 'name', 'price'];

			requiredProps.forEach(prop => {
				if (!book.hasOwnProperty(prop)) {
					throw new Error(`"${JSON.stringify(book)}" Неповні дані: відсутнє поле ${prop}`);
				}
			});

            const bookListItem = document.createElement('li');
            const bookName = document.createElement('h3');
            const bookAuthor = document.createElement('p');
            const bookPrice = document.createElement('p');

            bookName.innerText = book.name;
            bookAuthor.innerText = `Автор: ${book.author}`;
            bookPrice.innerText = `Ціна: ${book.price} грн.`;

            bookListItem.appendChild(bookName);
            bookListItem.appendChild(bookAuthor);
            bookListItem.appendChild(bookPrice);
            bookList.appendChild(bookListItem);
        } catch (err) {
            console.error(err.message);
        }
    });

    return bookList;
}
